-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2014 at 07:49 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hauteshot`
--

-- --------------------------------------------------------

--
-- Table structure for table `badges`
--

CREATE TABLE IF NOT EXISTS `badges` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `caption` varchar(150) NOT NULL,
  `contest_type` enum('weekly','monthly','custom') NOT NULL,
  `contest_status` enum('ended','active','coming') NOT NULL,
  `contest_start` datetime NOT NULL,
  `contest_end` datetime NOT NULL,
  `belonging_user` int(11) unsigned NOT NULL,
  `creator_id` int(11) unsigned NOT NULL,
  `winner_type` enum('Photo','User') NOT NULL,
  `winner_id` int(11) unsigned NOT NULL,
  `icon_path` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `badges`
--

INSERT INTO `badges` (`id`, `title`, `caption`, `contest_type`, `contest_status`, `contest_start`, `contest_end`, `belonging_user`, `creator_id`, `winner_type`, `winner_id`, `icon_path`, `created_at`, `updated_at`) VALUES
(1, 'Weekly Contest 26 November, 2014', 'Most promoted photo in the week.', 'weekly', 'ended', '2014-11-19 00:00:00', '2014-11-26 00:00:00', 1, 0, 'Photo', 3, 'uploads/badges/1.png', '2014-12-26 00:18:47', '2014-12-26 00:18:47'),
(2, 'Weekly Contest 4 December, 2014', 'Most promoted picture in the week gets this badge', 'weekly', 'ended', '2014-11-27 00:00:00', '2014-12-04 00:00:00', 3, 0, 'Photo', 5, 'uploads/badges/2.png', '2014-12-26 00:21:56', '2014-12-26 00:21:56'),
(3, 'Weekly Contest 14 December ', '', 'weekly', 'ended', '2014-12-05 00:00:00', '2014-12-14 00:00:00', 3, 0, 'Photo', 16, 'uploads/badges/3.png', '2014-12-26 00:24:02', '2014-12-26 00:24:02'),
(4, 'Weekly Most Promoted Photo ', 'Most promoted photo gets this badge.', 'weekly', 'active', '2014-12-21 00:00:00', '2014-12-28 23:59:59', 0, 0, '', 0, 'uploads/badges/1.png', '2014-12-26 00:26:51', '2014-12-26 00:26:51');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `caption` varchar(150) NOT NULL,
  `src` varchar(200) NOT NULL,
  `thumb_src` varchar(200) NOT NULL,
  `promotes_count` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `type` enum('feed','profile_pic') NOT NULL,
  `view_count` int(11) unsigned NOT NULL,
  `points` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `user_id`, `caption`, `src`, `thumb_src`, `promotes_count`, `created_at`, `updated_at`, `type`, `view_count`, `points`) VALUES
(1, 1, '', 'uploads/demo/1.jpg', 'uploads/demo/1.jpg', 0, '2014-12-25 19:42:29', '2014-12-25 19:42:29', 'feed', 20, 0),
(2, 1, '', 'uploads/demo/2.jpg', 'uploads/demo/2.jpg', 0, '2014-12-25 19:42:29', '2014-12-25 19:42:29', 'feed', 10, 0),
(3, 1, '', 'uploads/demo/3.jpg', 'uploads/demo/3.jpg', 0, '2014-10-23 02:00:00', '2014-11-05 10:00:00', 'feed', 220, 0),
(4, 2, '', 'uploads/demo/4.jpg', 'uploads/demo/4.jpg', 0, '2014-12-16 09:09:00', '2014-12-24 13:12:26', 'feed', 330, 0),
(5, 3, '', 'uploads/demo/5.jpg', 'uploads/demo/5.jpg', 0, '2014-11-12 00:00:00', '2014-12-02 00:00:00', 'feed', 110, 0),
(6, 4, '', 'uploads/demo/6.jpg', 'uploads/demo/6.jpg', 0, '2014-12-16 02:00:16', '2014-12-24 03:27:06', 'feed', 0, 0),
(7, 5, '', 'uploads/demo/7.jpg', 'uploads/demo/7.jpg', 0, '2014-11-12 12:31:05', '2014-12-17 07:15:14', 'feed', 110, 0),
(8, 6, '', 'uploads/demo/8.jpg', 'uploads/demo/8.jpg', 0, '2014-12-25 19:48:22', '2014-12-25 19:48:22', 'feed', 0, 0),
(9, 7, '', 'uploads/demo/9.jpg', 'uploads/demo/9.jpg', 0, '2014-12-25 19:49:38', '2014-12-25 19:49:38', 'feed', 0, 0),
(10, 8, '', 'uploads/demo/10.jpg', 'uploads/demo/10.jpg', 0, '2014-12-23 00:00:00', '2014-12-23 00:00:00', 'feed', 0, 0),
(11, 3, '', 'uploads/demo/11.jpg', 'uploads/demo/11.jpg', 0, '2014-09-18 03:17:00', '2014-12-09 00:00:00', 'feed', 0, 0),
(12, 3, '', 'uploads/demo/12.jpg', 'uploads/demo/12.jpg', 0, '2014-09-26 00:00:00', '2014-09-27 00:00:00', 'feed', 0, 0),
(13, 1, '', 'uploads/demo/13.jpg', 'uploads/demo/13.jpg', 4, '2014-12-25 19:52:27', '2014-12-25 19:52:27', 'feed', 300, 0),
(14, 3, '', 'uploads/demo/14.jpg', 'uploads/demo/14.jpg', 0, '2014-10-24 00:00:00', '2014-12-03 00:00:00', 'feed', 0, 0),
(15, 3, '', 'uploads/demo/15.jpg', 'uploads/demo/15.jpg', 0, '2014-11-12 00:00:00', '2014-12-10 00:00:00', 'feed', 0, 0),
(16, 3, '', 'uploads/demo/16.jpg', 'uploads/demo/16.jpg', 0, '2014-11-04 00:00:00', '2014-12-03 00:00:00', 'feed', 0, 0),
(17, 1, '', 'uploads/demo/17.jpg', 'uploads/demo/17.jpg', 0, '2014-12-02 00:00:00', '2014-12-02 00:00:00', 'feed', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE IF NOT EXISTS `tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `app_id` int(11) NOT NULL,
  `token` text NOT NULL,
  `expire_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`id`, `user_id`, `app_id`, `token`, `expire_at`, `created_at`, `updated_at`) VALUES
(8, 1, 2147483647, 'eyJpdiI6Ik5nNEtDTUNJRGVQZU52VHNaR29zbEE9PSIsInZhbHVlIjoiQlwvYWhLMDlVNW85TmFzcTRDZUt2eVJlYmZqRmxER204bTRGUjAzNGR2RFE9IiwibWFjIjoiNmMyOWE3ZDY1YjRhYzEwMmZlMDdmMTgwOTMxZDg3NjE5Y2RjOWQ2NmM3MzhjMWYwN2Y0NmJk', '2015-12-22 18:10:36', '2014-12-22 18:10:36', '2014-12-22 18:18:11'),
(9, 1, 2147483647, 'eyJpdiI6ImZQaWhMYWxCZjVuV1ljK2ZhVFBuNXc9PSIsInZhbHVlIjoiYWZXSUQ3bkxlams2RlRpTHl5S0EwZkc0Q3NNZnV2SWZJZnE3clJnbGFxWT0iLCJtYWMiOiIyM2QzYzYzYTdlNzE4N2MzM2Y4NDI1NjU2NjJlMGQ2MDMxNDcwYTFkYmQxYmU5Mzg3N2FjNjk5', '2015-12-22 18:18:11', '2014-12-22 18:18:11', '2014-12-22 18:18:11'),
(10, 1, 2147483647, 'eyJpdiI6Ik5tbkF0ZDFqNW9iMnpkTWxRem0xZVE9PSIsInZhbHVlIjoiTmlWRFFQb0ZpeXE3WnkrYXNvXC9VSVBVSHZYd2tBQUozOXNoRk5iSFBhaHc9IiwibWFjIjoiMDliNDJiOTEzYTE0ZTA2Y2YwMDdjN2UyYTViOWUwZWQxMTFmODVlYmQwNTg3MDc4ZTA5ZGUxMmIzYjkxYzczYiJ9', '2015-12-22 18:37:16', '2014-12-22 18:37:16', '2014-12-22 18:37:16'),
(11, 2, 2147483647, 'eyJpdiI6IlVKUlNLRXBFampZaVcrb08wMGVFWXc9PSIsInZhbHVlIjoiZVlcL28yemZ2NUZxS09yUHZWNmhsem5EVWRtV2FxWG11THhhMjQ1azJaVHc9IiwibWFjIjoiYjAzZjY2MDMzYjY2ZDdjMjZiMWVlZjk5ZjNhYTJhMTMyNjNkMzU3ZTBjMThlOTg3ZjFkMTA4NWNkMTIwNDg2MSJ9', '2015-12-22 19:27:36', '2014-12-22 19:27:36', '2014-12-22 19:27:36'),
(12, 3, 2147483647, 'eyJpdiI6IktSZlZOejNCXC9qTzNMZHljVnVjXC9vUT09IiwidmFsdWUiOiJ5bnJsQ3JjYzhQRXVsaUdKRWhyd0JBcFl6SnZ2QXUxRHJ4c2Q5SWRvTkZjPSIsIm1hYyI6IjM4Zjc1YmY1YzA0YjhjYTcwNWNjNTAzMDc1YWU5NGJhYWJlOWRiMWZiZDgwYTRlNTNkMDQ0YjQwMjEwODhkYTkifQ==', '2015-12-22 19:29:41', '2014-12-22 19:29:41', '2014-12-22 19:29:41'),
(13, 3, 214748364, 'eyJpdiI6IkF3SFlUc1RQejNLRldrUEYrcUVDeFE9PSIsInZhbHVlIjoicGJaUjg5QlpaQ2RtY096WHl6dDhhaVQ4b2RvSmlpcERjOUpJNHlZTjV2cz0iLCJtYWMiOiJhMzg2NDEwMTUwZGIzOTZlNzViNWJmYTBiNWFkMmZjYmNlZDRmNDViZGVlYmRmNTI0ODgzMDg2ZWNjM2FjYTEzIn0=', '2015-12-22 19:30:39', '2014-12-22 19:30:39', '2014-12-22 19:30:39'),
(14, 1, 2132441, 'eyJpdiI6ImE3SnZ6NVBmT3JwVnBUK3dQQkZ0NVE9PSIsInZhbHVlIjoiYzJLN3hcL3ZGRks2TmttVkhPc0RpNXlpMXNPeWhVV0gyOVZGbHh2NkxIUjQ9IiwibWFjIjoiOTY3YzAyN2JjYzM2MTc1MjhmZWRiNDE0MWJmYjIwYzhhM2FhNDFkZTg3MzAyOTgwOWZhOGUwMzVhNzgzZmFlMiJ9', '2015-12-23 07:40:18', '2014-12-23 07:40:18', '2014-12-23 07:40:18'),
(15, 1, 2324343, 'eyJpdiI6IlNDajRJdmdmYVpRNnVuYlpJVDRveEE9PSIsInZhbHVlIjoiWTY0ck8xRXFrYko1TjYxZGFkK3luNUxnalZ6UVUrajVZdXR3RUJmVlpPWT0iLCJtYWMiOiIyM2MzNzY3MWM2NTIwYWIzMTk5OTM0OTUzMDViMzJiNjM0YzA3YzZkZTgzNTRhOWE5MzViNzU3NThhMjJlN2Y0In0=', '2015-12-25 18:29:02', '2014-12-25 18:29:02', '2014-12-25 18:29:02'),
(16, 1, 2324343, 'eyJpdiI6Imk1dldcL1FPOU9rYW1LS25LYU9mQktBPT0iLCJ2YWx1ZSI6IkREcDNhMGxBSjFTdFwvR0Y5NDBRMTRNWlFNanh5OHNVc3NyWTN5SjJYY0xVPSIsIm1hYyI6ImEwMTkyYTUwNWY4NDM3NjFjMDczNTY1MDNmMTFlMzhkZDU1OWNiNThmYjJkMGUzMWY2ZjM1MDMzYmI2ZTZlNjAifQ==', '2015-12-25 18:29:18', '2014-12-25 18:29:18', '2014-12-25 18:29:18'),
(17, 1, 2324343, 'eyJpdiI6InpVaVoyNlU2ZlhYMVMyZEw3RTJEbUE9PSIsInZhbHVlIjoic2drR2hlNElOZjYyQWQzZzZmUEIyREpMdnpWWDh6YjZRY3VLVHp5cWFxVT0iLCJtYWMiOiIyYzM0YjUzYmQ0MmRhNzY2ZTdjMjU0MWMxY2YxOGVlNGU3ZmRkNTg1ZmRlZjVjNDgzMDk1MmZlOTY0OWNhZWE3In0=', '2015-12-25 18:34:07', '2014-12-25 18:34:07', '2014-12-25 18:34:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `profile_pic_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `activity_score` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `username`, `email`, `password`, `profile_pic_id`, `created_at`, `updated_at`, `activity_score`) VALUES
(1, 'Mahfuz Khan', 'mahfuz', 'mahfuz@gmail.com', '$2y$10$y9KfOvkxcyQGRYbyXIQH2.c.h/6jL/.UiEYFFQiboOAtJUTSjsxR2', 1, '2014-12-22 23:56:03', '2014-12-23 07:48:34', 0),
(2, 'Maiz ', 'mafuz', 'mafuz@gmail.com', '$2y$10$zqw1APLJ4KOeUVV5zcqC0O1/4fqB0PywOQ.YG2ckZMiKwCmJEDIni', 4, '2014-12-22 19:29:41', '2014-12-22 19:29:41', 0),
(3, 'Jhon', 'jhon', 'jhon@gmail.com', '4c25b32a72699ed712dfa80df77fc582', 5, '2014-12-25 18:22:54', '2014-12-25 18:22:54', 0),
(4, 'Miranda', 'miranda', 'miranda@gmail.com', '1ee1877c6655ecc71dfead311c771bd0', 6, '2014-12-25 18:22:54', '2014-12-25 18:22:54', 0),
(5, 'Shaharia', 'shaharia', 'shaharia@gmail.com', 'shaharia', 7, '2014-12-25 18:25:26', '2014-12-25 18:25:26', 0),
(6, 'Moudud Khan', 'moudud', 'moudud@gmail.com', 'moudud', 8, '2014-12-25 18:25:26', '2014-12-25 18:25:26', 0),
(7, 'Moin Khan', 'moin', 'moin@gmail.com', 'moin', 9, '2014-12-25 18:27:53', '2014-12-25 18:27:53', 0),
(8, 'Mukta', 'mukta', 'mukta@gmail.com', 'mukta', 10, '2014-12-25 18:27:53', '2014-12-25 18:27:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('promotes') NOT NULL,
  `target_type` enum('Photo','User') NOT NULL,
  `target_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `votes`
--

INSERT INTO `votes` (`id`, `type`, `target_type`, `target_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'promotes', 'Photo', 1, 3, '2014-12-23 14:16:00', '2014-12-23 14:16:00'),
(2, 'promotes', 'Photo', 13, 2, '2014-12-26 00:43:12', '2014-12-26 00:43:12'),
(3, 'promotes', 'Photo', 13, 3, '2014-12-26 00:43:12', '2014-12-26 00:43:12'),
(4, 'promotes', 'Photo', 13, 4, '2014-12-26 00:43:43', '2014-12-26 00:43:43'),
(5, 'promotes', 'Photo', 13, 5, '2014-12-26 00:43:43', '2014-12-26 00:43:43');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
