<?php
use HT\Services\File\ImageEditor;
use HT\Services\File\Uploader;

class PhotosController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    protected $editor;
    protected $uploader;

    public function __construct(ImageEditor $editor, Uploader $uploader)
    {
        $this->editor = $editor;
        $this->uploader = $uploader;
        //parent::__construct();
    }

    public function index()
    {
        return api_error('Not Supported!');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return api_error("Not Supported");
    }


    /**
     * Store a newly created resource in storage.
     * Input
     * user_id
     * caption
     * type
     * photo ---file
     * @return Response
     */
    public function store()
    {
        if (Token::$current->user->id != Input::get('user_id')) {
            return api_error('Unauthorized Request', 400);
        }
        $files = "";
        $data = Input::except('photo');
        if (Input::hasFile("photo")) {
            $this->uploader->setFiles(Input::file('photo'));
            $this->uploader->acceptUploadImages();
            $files = $this->uploader->getMovedFiles();
        }
        if (!is_array($files)) {
            return api_error('File Invalide');
        }
        $this->editor->ensureMaxSize($files[0], 1200, 1000);
        $data['src'] = $files[0]->getPublicPath();
        $data['thumb_src'] = $thumb->getPublicPath();
        $data['status'] = Photo::STATUS_PUBLISHED;
        $data['type'] = Input::get('type') ?: Photo::TYPE_FEED;
        $validator = Validator::make($data, array(
            'user_id' => 'required|exists:users,id',
            'caption' => '',
            'src' => 'required',
            'type' => 'required|in:' . Photo::TYPE_PP . ',' . Photo::TYPE_FEED,
        ));
        if ($validator->fails()) {
            return api_error($validator->messages()->first());
        }
        $photo = Photo::create($data);
        return api_success($photo);
    }
    public function createPhotoObj()
    {
        $data = [
            "user_id" => Token::$current->user->id,
            "caption" => "",
            "src" => Input::get('file_path'),
            "status" => Photo::STATUS_DRAFT,
            "in_progress" => true,
        ];
        $photo = Photo::create($data);
        return api_success($photo);
    }
    public function uploadPhotoBinary($id){
        $photo = Photo::findOrFail($id);
        if($photo->user_id!=Token::$current->user->id){
            return api_error();
        }
        if(!Input::hasFile('photo')){
            return;
        }
        $this->uploader->setFiles(Input::file('photo'));
        $this->uploader->acceptUploadImages();
        $files = $this->uploader->getMovedFiles();
        if (!is_array($files)) {
            return api_error('File Invalide');
        }
        $photo->src = $files[0]->getPublicPath();
        $photo->status = Photo::STATUS_PUBLISHED;
        $photo->in_progress = false;
        $photo->save();
        return api_success($photo);
    }

    public function displayPhoto($id, $type, $width, $height, $filename, $ext)
    {
        $photo = Photo::findOrFail($id);
        $width = (int)$width;
        $height = (int)$height;
        $filepath = base_path() . "/caches/" . $id . "." . $type . "." . $width . "x" . $height . ".jpg";
        if (!file_exists($filepath)) {
            $img = Image::make($photo->src);
            if ($type == "t") {
                $thumb = $img->fit($width ?: 1000, $height ?: null, function ($constraint) {
                    $constraint->upsize();
                });
            } else {
                $thumb = $img->resize($width ?: null, $height ?: null, function ($constraint) {
                    $constraint->upsize();
                    $constraint->aspectRatio();
                });
            }
            $thumb->save($filepath, 90);
        }
        return flush_file($filepath);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $photo = Photo::findOrFail($id)->load('user');
        $photo->view_count += 1;
        $photo->save();
        //dd($photo);
        return api_success($photo);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return api_error('Not Supported');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $photo = Photo::findOrFail($id);
        if(Input::has('status'))
        $photo->status = Input::get('status');
        if(Input::has('caption')){
            $caption = Input::get('caption');
            $photo->caption = str_replace(["\n","\r","\n\r"], " ", $caption);
        }
        $photo->save();
        return api_success($photo);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
