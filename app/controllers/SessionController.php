<?php
class SessionController extends BaseController{
	public function submitLogin(){
		$username = Input::get('username');
		$password = Input::get('password');
		$app_id = Input::get('app_id');
		if(empty($username)||empty($password)||empty($app_id)){
			return api_error();
		}
		if(Auth::validate(compact('username','password'))){
			$user = User::where('username','=',$username)->first();
			$token = Token::generateToken($user->id, $app_id);
			return api_success($token);
		}else{
			return api_error('Invalid Credentials');
		}
	}
	public function submitRegister(){
		$app_id = Input::get('app_id');
		if(!$app_id){
			return api_error();
		}
		$validator = Validator::make(Input::all(), array(
			'username' => 'required|min:3|max:12|alpha_dash|unique:users,username',
			'email' => 'required|min:5|max:50|email|unique:users,email',
			'password' => 'required|min:6|max:50',
			'full_name' => ''
			));
		if($validator->fails()){
			return api_error($validator->messages()->first());
		}
		$data = Input::all();
		$data['password'] = Hash::make($data['password']);
		$user = User::create($data);
		if(!$user){
			return api_error('Unknown Error');
		}
		$token = Token::generateToken($user->id,$app_id);
		return api_success($token);
	}
}