<?php
use HT\Services\File\ImageEditor;
use HT\Services\File\Uploader;

class UsersController extends \BaseController {


	protected $editor;
	protected $uploader;

	public function __construct(ImageEditor $editor, Uploader $uploader)
	{
		$this->editor = $editor;
		$this->uploader = $uploader;
        //parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return api_error('Not Supported Method Call');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return api_error('Not Supported Method Call');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return api_error('Not Supported Method Call');
	}

	public function UpdateCurrentUserData(){
		$data = Input::all();
		$user = Token::$current->user;
		$user->update($data);
		return api_success($user);
	}
	public function UpdateCurrentUserProfilePic(){
		$user = Token::$current->user;
		if(!Input::hasFile('photo')){
			return;
		}
		$this->uploader->setFiles(Input::file('photo'));
		$this->uploader->acceptUploadImages();
		$files = $this->uploader->getMovedFiles();
		if (!is_array($files)) {
			return api_error('File Invalide');
		}
		$photo = Photo::create([
			'user_id' => $user->id,
			'caption' => "",
			"src" => $files[0]->getPublicPath(),
			"status" => Photo::STATUS_DRAFT,
			"in_progress" => false,
			]);
		if(!$photo){
			return api_error();
		}
		$user->profile_pic_id = $photo->id;
		$user->save();
		return api_success($user);
	}
	public function displayPP($id, $width, $height){
		$user = User::findOrFail($id);
		$width = $width?:200;
		$height = $height?:200;
		$filepath = base_path() . "/caches/user." . $user->profile_pic_id . "."  . $width . "x" . $height . ".jpg";
		$photo = Photo::find($user->profile_pic_id);
		if(!$photo){
			return flush_file(base_path()."/public/images/avater.png");
		}
		if (!file_exists($filepath)) {
			$img = Image::make($photo->src);
			$thumb = $img->resize($width ?: null, $height ?: null, function ($constraint) {
				$constraint->upsize();
				$constraint->aspectRatio();
			});
			$thumb->save($filepath, 90);
		}
		return flush_file($filepath);
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);
		$user->loadAlbums()->loadBadgesWon();
		return api_success($user);
	}

	public function UpdateUserPassword(){
		$user = Token::$current->user;
		$oldpass = Input::get("old_pass");
		$newpass = Input::get("new_pass");
		if(empty($newpass)||empty($oldpass)){
			return api_error();
		}
		if(Hash::check($oldpass, $user->password)){
			$user->password = Hash::make($newpass);
			$user->save();
			return api_success($user);
		}else{
			return api_error("Old password not matched!");
		}
	}

	public function albums($id){
		$user = User::findOrFail($id);
		return api_success($user->getAlbums());
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return api_error('Not Supported Method Call');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::findOrFail($id);
		if($user->id!==Token::$current->user->id){
			return api_error('Not Authorized',401);
		}
		$data = Input::all();
		if(isset($data['username'])&&$data['username']==$user->username){
			unset($data['username']);
		}
		if(isset($data['email'])&&$data['email']==$user->email){
			unset($data['email']);
		}
		if(isset($data['password'])&&empty($data['password'])){
			unset($data['password']);
		}
		$validator = Validator::make($data, array(
			'username' => 'sometimes|required|min:3|max:12|alpha_dash|unique:users,username',
			'email' => 'sometimes|required|min:5|max:50|email|unique:users,email',
			'password' => 'sometimes|required|min:6|max:50',
			'full_name' => '',
			'profile_pic_id' => 'sometimes|required|exists:photos,id'
			));
		if($validator->fails()){
			return api_error($validator->messages()->first());
		}
		if(isset($data['password'])){
			$data['password'] = Hash::make($data['password']);
		}
		$user->update($data);
		return api_success($user);
	}

	public function followUser($id){
		$user = User::findOrFail($id);
		if($user->id==Token::$current->user->id){
			return api_error("You should already follow yourself man!");
		}
		$tryFollow = $user->followers()->attach(Token::$current->user->id);
		if($tryFollow){
			return api_success("done!");
		}else{
			return api_error("Something went wrong!");
		}
	}

	public function unFollowUser($id){
		$user = User::findOrFail($id);
		$user->followers()->detach(Token::$current->user->id);
		return api_success("done!");
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return api_error('Not Supported Method Call');
	}

	public function fileReport(){
		try{

			Mail::send("emails.file-report", array("input"=>Input::all()), function($message){
				$message->to("makhan075@gmail.com");
			});
		}catch(Exception $e){
			dd($e)
		}
		return api_success("done");
	}

}
