<?php

class PhotosVotesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($photoId)
	{
		$photo = Photo::findOrFail($photoId);
		return api_success($photo->promotes()->with('user')->get());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return api_error();
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($photoId)
	{
		$photo = Photo::findOrFail($photoId);
		$user = Token::$current->user;
		$type = Input::get('type')?:Vote::TYPE_PROMOTES;
		$data = array('target_type'=>'Photo','target_id'=>$photoId,'user_id'=>$user->id,'type'=>$type);
		if(Vote::where($data)->count()>0){
			return api_error('Action already done');
		}
		if(Vote::create($data)){
			$photo->promotes_count += 1;
			$photo->save();
			return api_success($photo->with('user'));
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return api_error();
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return api_error();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return api_error();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($photoId, $voteId)
	{
		$photo = Photo::findOrFail($photoId);
		$user = Token::$current->user;
		$type = Input::get('type')?:Vote::TYPE_PROMOTES;
		$data = array('target_type'=>'Photo','target_id'=>$photoId,'user_id'=>$user->id,'type'=>$type);
		$q = Vote::where($data);
		if($q->count()<1){
			return api_error();
		}
		if($q->delete()){
			$photo->promotes_count -= 1;
			$photo->save();
			return api_success($photo->with('user'));
		}
	}


}
