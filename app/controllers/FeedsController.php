<?php

use Carbon\Carbon;

class FeedsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		Log::error(Input::get("after_offset"));
		Log::error(Input::get("before_offset"));
		if(Input::has("after_offset")){
			$dt1 = Carbon::now();
			$dt2 = Carbon::parse(Input::get("after_offset"));
		}
		if(Input::has("before_offset")){
			$dt1 = Carbon::parse(Input::get("before_offset"));
			$dt2 = Carbon::parse("2014-07-30 00:00:00");
		}
		if(isset($dt1)&&isset($dt2)){
			Log::info($dt1);
			$photos = Photo::published()->betweenDates([$dt2, $dt1]);
		}else{
			$photos = Photo::published();
		}
		$photos = $photos->with('user')->orderBy('created_at','desc');
		$photos = $photos->simplePaginate(Input::get('pagging',100));
		// Log::error(Photo::find(1)->toArray());
		return api_success($photos->toArray()['data']);
	}
	public function getUsersFeed($id){
		$user = User::findOrFail($id);
		$dt1 = Input::get('date1');
		$dt2 = Input::get('date2');
		if(empty($dt1)||empty($dt2)){
			$photos = $user->photos();
		}else{	
			$photos = $user->photos()->published()->betweenDates(array(Carbon::parse($dt1), Carbon::parse($dt2)));
		}
		$photos = $photos->simplePaginate(Input::get('pagging',100));
		return api_success($photos->toArray()['data']);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return api_error();
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return api_error();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return api_error();
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return api_error();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return api_error();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return api_error();
	}


}
