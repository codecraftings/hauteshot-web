<?php

use HT\Contests\ContestRepo;
class ContestsController extends \BaseController {

	protected $contests;

	public function __construct(ContestRepo $contests){
		$this->contests = $contests;
	}
	public function getActiveContests(){
		$contests = $this->contests->getActiveContests();
		foreach ($contests as $contest) {
			// dd($contest->total_entry);
			$contest->loadRankList();
		}
		return api_success($contests);
	}

	public function getLatestBadges(){
		$badges = Badge::query()->orderBy("id",'desc')->with('user','contest','winner')->take(20)->get()->toArray();
		return api_success($badges);
	}
	public function getUsersBadges($id){
		$user = User::findOrFail($id);
		return api_success($user->allBadges()->orderBy('id','desc')->with('winner','user', 'contest')->get());
	}

}
