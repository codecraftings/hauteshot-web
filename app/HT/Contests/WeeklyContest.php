<?php namespace HT\Contests;
use Carbon\Carbon;
class WeeklyContest extends PhotoContest{
	public static function Active(){
		$q = \Contest::where('manager_class','=',"HT\Contests\WeeklyContest")
						->where('status','=', \Contest::STATUS_ACTIVE);
		if($q->count()<1){
			return false;
		}
		return $q->first();
	}
	public static function CreateNew(){
		if(self::Active()!=false){
			return false;
		}
		$now = Carbon::now();
		$data = [
			"manager_class" => "HT\Contests\WeeklyContest",
			"title" => "Weekly Best Photo",
			'start_date' => $now->copy()->startOfWeek(),
			'end_date'  => $now->copy()->endOfWeek(),
			'status' => \Contest::STATUS_ACTIVE,
			'badge_icon_path' => "uploads/badges/3.png",
			'description'  => "Most promoted photo in a week wins this contest"
		];
		$contest = \Contest::create($data);

		return $contest;
	}
	public function getFriendlyDateInfo(){
		return $this->model->start_date->format("j")."-".$this->model->end_date->format("j F, Y");
	}
	public function getAwardCaption($place){
		$caption = "Best Photo in ".$this->getFriendlyDateInfo();
		return $caption;		
	}
	public function getAwardIcon($place){
		return $this->model->badge_icon_path;
	}
	public function awardWinners(){
		$this->createAward(1);
	}
}