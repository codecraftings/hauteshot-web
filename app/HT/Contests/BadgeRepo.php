<?php namespace HT\Contests;

use Badge;
/**
* 
*/
class BadgeRepo
{
	protected $badges;
	public function __construct(Badge $badge)
	{
		$this->badges = $badge;
	}
	public function addBadgeForPhoto($photo, $contest){
		$data = [
			'caption' => $contest->getBadgeTitle(),
			'winner_type' => 'Photo',
			'winner_id' => $photo->id,
			'belonging_user' => $photo->user->id,
			'contest_id' => $contest->id,
			'icon_path' => $contest->badge_icon_path
		];
		$badge = $this->badges->create($data);
		return $badge;
	}
}