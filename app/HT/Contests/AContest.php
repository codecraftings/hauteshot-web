<?php namespace HT\Contests;

abstract class AContest{
	protected $model;
	public function __construct(\Contest $model){
		$this->model = $model;
	}
	
	public function endContest(){
		$this->model->status = \Contest::STATUS_ENDED;
		return $this->model->save();
	}
	protected function getWinner($place = 1){
		$rank_list = $this->getRankList();
		if(isset($rank_list[$place-1]))
			return $rank_list[$place-1];
		else
			return null;
	}
	abstract function getTotalEntry();
	abstract function getRankList();
	abstract function getAwardCaption($place);
	abstract function getAwardIcon($place);
	abstract function createAward($place);
	abstract function awardWinners();
	abstract function getFriendlyDateInfo();
}