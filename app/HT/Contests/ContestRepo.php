<?php namespace HT\Contests;

use Contest;
use Carbon\Carbon;
class ContestRepo{
	protected $contests;
	public function __construct(Contest $contests){
		$this->contests = $contests;
	}
	public function getActiveContests(){
		return $this->contests->where("status",'=',Contest::STATUS_ACTIVE)->get();
	}
	public function getContestRankList($contest){
		$contest->loadRankList();
		return $contest->rank_list;
	}
}