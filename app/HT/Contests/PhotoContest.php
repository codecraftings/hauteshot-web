<?php namespace HT\Contests;

abstract class PhotoContest extends AContest{
	public function createAward($place=1){
		$photo = $this->getWinner($place);
		if($photo==null){
			return;
		}
		$data = [
			'caption' => $this->getAwardCaption($place),
			'winner_type' => 'Photo',
			'winner_id' => $photo->id,
			'belonging_user' => $photo->user->id,
			'contest_id' => $this->model->id,
			'icon_path' => $this->getAwardIcon($place)
		];
		$badge = \Badge::create($data);
		return $badge;
	}
	public function getRankList(){
		return \Photo::betweenDates([$this->model->start_date, $this->model->end_date])->orderBy("promotes_count","desc")->take(10)->get();
	}
	public function getTotalEntry(){
		return \Photo::betweenDates([$this->model->start_date, $this->model->end_date])->count();
	}
}