<?php namespace HT\Contests;
use Carbon\Carbon;
class MonthlyContest extends PhotoContest{
	public static function Active(){
		$q = \Contest::where('manager_class','=',"HT\Contests\MonthlyContest")
						->where('status','=', \Contest::STATUS_ACTIVE);
		if($q->count()<1){
			return false;
		}
		return $q->first();
	}
	public static function CreateNew(){
		if(self::Active()!=false){
			return false;
		}
		$now = Carbon::now();
		$data = [
			"manager_class" => "HT\Contests\MonthlyContest",
			"title" => "Monthly Best Photo",
			'start_date' => $now->copy()->startOfMonth(),
			'end_date'  => $now->copy()->endOfMonth(),
			'status' => \Contest::STATUS_ACTIVE,
			'badge_icon_path' => "uploads/badges/1.png",
			'description'  => "Most promoted photo in a month wins this contest"
		];
		$contest = \Contest::create($data);

		return $contest;
	}
	public function getFriendlyDateInfo(){
		return $this->model->start_date->format("F, Y");
	}
	public function getAwardCaption($place){
		$caption = "Best Photo in ".$this->model->start_date->format("F, Y");
		return $caption;		
	}
	public function getAwardIcon($place){
		return $this->model->badge_icon_path;
	}
	public function awardWinners(){
		$this->createAward(1);
	}
}