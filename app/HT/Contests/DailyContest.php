<?php namespace HT\Contests;
use Carbon\Carbon;
use Contest;
class DailyContest extends PhotoContest{
	public static function Active(){
		$q = \Contest::where('manager_class','=',"HT\Contests\DailyContest")
						->where('status','=', \Contest::STATUS_ACTIVE);
		if($q->count()<1){
			return false;
		}
		return $q->first();
	}
	public static function CreateNew(){
		if(self::Active()!=false){
			return false;
		}
		$now = Carbon::now();
		$data = [
			"manager_class" => "HT\Contests\DailyContest",
			"title" => "Daily Best Photo",
			'start_date' => $now->copy()->startOfDay(),
			'end_date'  => $now->copy()->endOfDay(),
			'status' => \Contest::STATUS_ACTIVE,
			'badge_icon_path' => "uploads/badges/2.png",
			'description'  => "Most promoted photo in a day wins this contest"
		];
		$contest = \Contest::create($data);

		return $contest;
	}
	public function getFriendlyDateInfo(){
		return $this->model->end_date->format("j F, Y");
	}
	public function getAwardCaption($place){
		$caption = "Best Photo on ".$this->getFriendlyDateInfo();
		return $caption;		
	}
	public function getAwardIcon($place){
		return $this->model->badge_icon_path;
	}
	public function awardWinners(){
		$this->createAward(1);
	}
}