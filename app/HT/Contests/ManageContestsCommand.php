<?php namespace HT\Contests;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;

class ManageContestsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'contests:manage';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	protected $repo;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ContestRepo $repo)
	{
		$this->repo = $repo;
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->endDueContests();
		$this->startNewContests();
	}
	private function endDueContests(){
		$contests = $this->repo->getActiveContests();
		$this->info($contests->count()." contests found. checking if date over.");
		foreach ($contests as $contest) {
			$this->info("checking..".$contest->title);
			if($contest->end_date->isPast()){
				$this->info("ending contest..");
				$contest->manager()->endContest();
				$this->info("status changed.. awarding winners.");
				try{
					$contest->manager()->awardWinners();
				}catch(\ErrorException $e){
					$this->info(json_encode($e->getTrace()));
				}
				$this->info("winners awarded.. next..");
			}
		}
	}
	private function startNewContests(){
		if(false==MonthlyContest::Active()){
			$this->info("creating new MontlyContest");
			MonthlyContest::CreateNew();
		}
		if(false==WeeklyContest::Active()){
			$this->info("creating new WeeklyContest");
			WeeklyContest::CreateNew();
		}
		if(false==DailyContest::Active()){
			$this->info("creating new DailyContest");
			DailyContest::CreateNew();
		}
	}
	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
