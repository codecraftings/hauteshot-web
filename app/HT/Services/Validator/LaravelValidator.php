<?php
namespace HT\Services\Validator;
use Illuminate\Validation\Factory;
abstract class LaravelValidator extends AbstractValidator{
	public function __construct(Factory $validator){
		$this->validator = $validator;
	}
	public function passes(){
		$validator = $this->validator->make($this->data, $this->rules);
		if($validator->fails()){
			$this->errors = $validator->messages();
			return false;
		}
		return true;
	}
}