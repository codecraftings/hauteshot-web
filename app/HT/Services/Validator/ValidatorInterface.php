<?php
namespace HT\Services\Validator;
interface ValidatorInterface{
	public function with(array $data);
	public function passes();
	public function errors();
}