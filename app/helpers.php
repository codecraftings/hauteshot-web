<?php
use Symfony\Component\HttpFoundation\BinaryFileResponse;

function api_error($msg ="Bad Request", $code=400, $trace=null){
	$data = array(
		'status' => $code,
		'error'=>array('message'=>$msg)
		);
	if($trace!=null){
		$data['trace'] = $trace;
	}
	return Response::json($data, $code);
}

function api_success($data, $code=200){
	return Response::json(array(
		'status' => $code,
		'data' => $data
		), $code);
}
function get_file_mime($file_path){
	$finfo = finfo_open(FILEINFO_MIME_TYPE);
	$mime = finfo_file($finfo, $file_path);
	finfo_close($finfo);
	return $mime;
}
function flush_file($file_path){
	//dd(get_file_mime($file_path));
	$headers = array(
		'Cache-Control'             => 'max-age:3600',
		'Content-Type'              => get_file_mime($file_path),
		'Content-Transfer-Encoding' => 'binary',
		'Content-Disposition'       => 'inline',
		'Content-Length'			=> File::size($file_path),
		'ETag'						=> md5($file_path)
	);
	//dd($headers);
//	$file = fopen($file_path,'r');
	return new BinaryFileResponse($file_path, 200, $headers);
	/*return Response::stream(function() use ($file){
		fpassthru($file);
	}, 200, $headers);*/
}