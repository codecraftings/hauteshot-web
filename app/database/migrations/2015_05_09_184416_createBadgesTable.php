<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBadgesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("badges", function($table){
			$table->bigIncrements("id")->unsigned();
			$table->string("caption");
			$table->char("winner_type",30);
			$table->bigInteger("winner_id")->unsigned();
			$table->string("icon_path");
			$table->bigInteger("contest_id")->unsigned();
			$table->bigInteger("belonging_user")->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("badges");
	}

}
