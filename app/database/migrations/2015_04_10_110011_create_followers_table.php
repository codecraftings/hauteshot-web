<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("followers", function($table){
			$table->bigIncrements('id');
			$table->timestamps();
			$table->bigInteger('user_id')->unsigned();
			$table->bigInteger('follower_id')->unsigned();
			$table->char('status',10);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("followers");
	}

}
