<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("votes", function($table){
			$table->bigIncrements("id")->unsigned();
			$table->char("type", 30);
			$table->char("target_type", 30);
			$table->bigInteger("target_id")->unsigned();
			$table->bigInteger("user_id")->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("votes");
	}

}
