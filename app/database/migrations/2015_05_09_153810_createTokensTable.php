<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("tokens", function($table){
			$table->bigIncrements("id")->unsigned();
			$table->bigInteger("user_id")->unsigned();
			$table->integer("app_id");
			$table->text("token");
			$table->dateTime("expire_at");
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("tokens");
	}

}
