<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("photos", function($table){
			$table->bigIncrements("id")->unsigned();
			$table->char("type", 30);
			$table->char("status", 30);
			$table->bigInteger("user_id")->unsigned();
			$table->text("caption");
			$table->boolean("in_progress");
			$table->text("src");
			$table->bigInteger("promotes_count")->unsigned();
			$table->bigInteger("view_count")->unsigned();
			$table->bigInteger("points")->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("photos");
	}

}
