<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("contests", function($table){
			$table->bigIncrements("id");
			$table->string("title");
			$table->text("description");
			$table->string("manager_class");
			$table->string("badge_icon_path");
			$table->dateTime("start_date");
			$table->dateTime("end_date");
			$table->char("status", 20);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("contests");
	}

}
