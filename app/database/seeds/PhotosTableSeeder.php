<?php

class PhotosTableSeeder extends Seeder{
	public function run(){
		for($i=1;$i<18;$i++){
			$this->create("uploads/demo/".$i.".jpg", mt_rand(1, 3));
		}
	}
	private function create($src, $user_id){
		$date = Carbon\Carbon::now()->subHours(mt_rand(1, 200));
		Photo::create([
				'type' => Photo::TYPE_FEED,
				'status'=> Photo::STATUS_PUBLISHED,
				'user_id'=> $user_id,
				'src'=> $src,
				'caption'=> "demo caption and #demo#tag#caption#hauteshot",
				'promotes_count' => mt_rand(0, 10),
				'created_at' => $date,
				'updated_at' => $date
			]);
	}
}