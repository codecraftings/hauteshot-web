<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		$this->call('PhotosTableSeeder');
		$this->call('ContestTableSeeder');
		$this->call('BadgesTableSeeder');
		HT\Contests\DailyContest::CreateNew();
		HT\Contests\WeeklyContest::CreateNew();
		HT\Contests\MonthlyContest::CreateNew();
	}

}
