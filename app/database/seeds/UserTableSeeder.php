<?php

class UserTableSeeder extends Seeder{
	public function run(){
		User::create([
				"full_name" => "Mahfuz Khan",
				"username"	=> "mahfuz",
				"email"		=> "makhan075@gmail.com",
				"password"	=>	Hash::make("bangla"),
				"profile_pic_id" => 1
			]);
		User::create([
				"full_name" => "Moudud Khan",
				"username"	=> "moudud",
				"email"		=> "moudud@gmail.com",
				"password"	=>	Hash::make("bangla"),
				"profile_pic_id" => 3
			]);
		User::create([
				"full_name" => "Mahbub Khan",
				"username"	=> "mahbub",
				"email"		=> "mahbub@gmail.com",
				"password"	=>	Hash::make("bangla"),
				"profile_pic_id" => 5
			]);
	}
}