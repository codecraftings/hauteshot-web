<?php

class ContestTableSeeder extends Seeder{

	public function run(){
		for($i=1;$i<=10;$i++){
			$this->create(Carbon\Carbon::now()->subDays(30*$i));
		}
	}
	private function create($dt){
		$this->save(HT\Contests\DailyContest::CreateNew(), $dt->copy()->startOfDay(), $dt->copy()->endOfDay());
		$this->save(HT\Contests\WeeklyContest::CreateNew(), $dt->copy()->startOfWeek(), $dt->copy()->endOfWeek());
		$this->save(HT\Contests\MonthlyContest::CreateNew(), $dt->copy()->startOfMonth(), $dt->copy()->endOfMonth());
	}
	private function save($contest, $dt1, $dt2){
		$contest->start_date = $dt1;
		$contest->end_date = $dt2;
		$contest->status = \Contest::STATUS_ENDED;
		$contest->save();
	}
}