<?php

class BadgesTableSeeder extends Seeder{

	public function run(){
		$this->create();
		$this->create();
		$this->create();
		$this->create();
		$this->create();
		$this->create();
		$this->create();
	}
	public function create(){
		$photo = \Photo::find(mt_rand(1, 10));
		$contest = \Contest::find(mt_rand(1,9));
		$data = [
			'caption' => $contest->title,
			'winner_type' => 'Photo',
			'winner_id' => $photo->id,
			'belonging_user' => $photo->user->id,
			'contest_id' => $contest->id,
			'icon_path' => $contest->badge_icon_path
		];
		\Badge::create($data);
	}
}