<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

$monolog = Log::getMonolog();
$syslog = new \Monolog\Handler\SyslogHandler('papertrail');
$formatter = new \Monolog\Formatter\LineFormatter('%channel%.%level_name%: %message% %extra%');
$syslog->setFormatter($formatter);

$monolog->pushHandler($syslog);

Route::get('/', function()
{
	return Response::make("Commin soon..");
});
Route::get('test', function(){
	echo User::findOrFail(1000)->getAlbums();
	echo "Yey!! it works from mac223332!!";
});
Route::get('users/{id}/{width}x{height}/picture.jpg', 'UsersController@displayPP');
Route::get('resource/photo/{id}/{type}/{width}x{height}/{filename}.{ext}', 'PhotosController@displayPhoto');
Route::post('api/v1/login','SessionController@submitLogin');
Route::post('api/v1/register','SessionController@submitRegister');
Route::get('report', 'UsersController@fileReport');
Route::group(array('prefix'=>'api/v1','before'=>'verify_token'), function(){
	Route::post('report', 'UsersController@fileReport');
	Route::post('me', 'UsersController@UpdateCurrentUserData');
	Route::post('me/picture', 'UsersController@UpdateCurrentUserProfilePic');
	Route::post('me/password', 'UsersController@UpdateUserPassword');
	Route::get('users/{id}/albums','UsersController@albums');
	Route::get('users/{id}/feeds','FeedsController@getUsersFeed');
	Route::post('users/{id}/follow','UsersController@followUser');
	Route::delete('users/{id}/follow','UsersController@unFollowUser');
	Route::resource('users','UsersController');
	Route::post('photos/create_dummy','PhotosController@createPhotoObj');
	Route::post('photos/{id}/upload_photo','PhotosController@uploadPhotoBinary');
	Route::post("photos/{id}/update", 'PhotosController@update');
	Route::resource('photos','PhotosController');
	Route::resource('photos.votes','PhotosVotesController');
	//Route::get('feeds/home','FeedsController@getHome');
	Route::resource('feeds','FeedsController');
	Route::get('users/{id}/badges','ContestsController@getUsersBadges');
	Route::get('contests/active','ContestsController@getActiveContests');
	Route::get('contests/badges','ContestsController@getLatestBadges');
});
