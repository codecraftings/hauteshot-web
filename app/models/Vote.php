<?php
/*
id
type
target_type
target_id
user_id
created_at
updated_at
*/
class Vote extends Eloquent{
	protected $table = "votes";
	const TYPE_PROMOTES = "promotes";
	protected $fillable = array('type','target_type','target_id','user_id');
	public function target(){
		return $this->MorphTo('target');
	}
	public function scopePromotes($query){
		return $query->where('type',Vote::TYPE_PROMOTES);
	}
	public function user(){
		return $this->belongsTo('User');
	}
}