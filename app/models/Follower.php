<?php
/*
DB Fields
----
id
user_id
follower_id
status
created_at
updated_at
*/
class Follower extends Eloquent{
	protected $table = "followers";
	protected $fillable = array('user_id','follower_id','status');
}