<?php
/*
DB Fields
---------
id
user_id
app_id
token
expire_at
created_at
updated_at
*/
class Token extends Eloquent{
	protected $table = "tokens";
	protected $fillable = array('user_id','app_id','token','expire_at');
	protected $hidden = array('id','created_at','updated_at');
	public static $current;
	public function getDates(){
		return ['expire_at','created_at','updated_at'];
	}
	public static function generateToken($user_id, $app_id){
		$token = Crypt::encrypt(str_random(10).'&'.$app_id.'&'.$user_id);
		$expire_at = Carbon::now()->addYear(1);
		//Token::query()->where('app_id','=',$app_id)->where('user_id','=',$user_id)->delete();
		$token = Token::create(compact('token','app_id','user_id','expire_at'));
		$token->load('user');
		return $token;
	}
	public function user(){
		return $this->belongsTo('User');
	}
}