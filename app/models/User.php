<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
/*
DB Fileds
------------
id
full_name
username
email
password
profile_pic_id
created_at
updated_at
*/

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $fillable = array('full_name','username','email','profile_pic_id', 'password');
	protected $appends = array('profile_pic_url', 'photos_count', 'promotes_count', 'is_followed', 'albums','badges_won');
	private $_albums = array();
	private $_badges_won = array();
	public function getAlbumsAttribute(){
		return $this->_albums;
	}
	public function getBadgesWonAttribute(){
		return $this->_badges_won;
	}
	public function getIsFollowedAttribute(){
		if(!Token::$current){
			return false;
		}
		if($this->followers->contains(Token::$current->user)){
			return true;
		}else{
			return false;
		}
	}
	public function getProfilePicUrlAttribute(){
		$photo = Photo::find($this->profile_pic_id);
		if(!$photo){
			return url('images/avatar.png');
		}
		return $photo->thumb_url;
	}
	public function followers(){
		return $this->belongsToMany("User","followers","user_id","follower_id")->withTimestamps();
	}
	public function photos(){
		return $this->hasMany('Photo');
	}
	public function getPhotosCountAttribute(){
		return $this->photos()->count();
	}
	public function getPromotesCountAttribute(){
		return 1000;
	}
	public function loadAlbums(){
		$this->_albums = $this->getAlbums();
		return $this;
	}
	public function getAlbums(){
		$first = $this->photos()->orderBy('created_at')->first();
		$last = $this->photos()->orderBy('created_at','desc')->first();
		$m = $last->created_at->month;
		$y = $last->created_at->year;
		$albums = array();
		for($j=$y;$j>=$first->created_at->year;$j--){

			for($i=$m;$i>0;$i--){
				$dt = Carbon::parse($j.'-'.$i.'-1');
				//echo $dt;
				$dt1 = $dt->copy()->startOfMonth();
				$dt2 = $dt->copy()->endOfMonth();
				$album = Album::createFromRange($this, $dt1, $dt2);
				if(count($album->feature_photos)<1){
					continue;
				}
				array_push($albums, $album);
			}
			$m = 12;
		}
		return $albums;
	}
	public function loadBadgesWon(){
		$this->_badges_won = $this->allBadges;
		return $this;
	}
	public function badges(){
		return $this->morphMany('Badge','winner');
	}
	public function allBadges(){
		return $this->hasMany('Badge','belonging_user');
	}
}
