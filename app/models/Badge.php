<?php
/*
DB Fields
-----------
id
caption
winner_type
winner_id
icon_path
contest_id
belonging_user
created_at
updated_at
*/
class Badge extends Eloquent{
	protected $table = "badges";
	protected $fillable = array('caption','winner_type','winner_id','icon_path', 'contest_id','belonging_user');
	protected $appends = array('icon_url');
	
	public function getIconUrlAttribute(){
		return url($this->icon_path);
	}
	public function winner(){
		return $this->morphTo('winner');
	}
	public function contest(){
		return $this->belongsTo('Contest');
	}
	public function user(){
		return $this->belongsTo('User','belonging_user');
	}
}