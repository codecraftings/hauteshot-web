<?php
/*
DB Fields
------------
id
title
description
manager_class
badge_icon_path
start_date
end_date
status
created_at
updated_at
*/
class Contest extends Eloquent{
	protected $table = "contests";

	const STATUS_ACTIVE = "active";
	const STATUS_ENDED = "ended";
	const STATUS_COMING = "coming";

	protected $fillable = ["title","description","manager_class","badge_icon_path","start_date", "end_date","status"];

	protected $appends = ['rank_list', 'total_entry', 'icon_url', 'friendly_date_info'];
	private $_rank_list = [];

	public function getIconUrlAttribute(){
		return url($this->badge_icon_path);
	}
	public function getTotalEntryAttribute(){
		return $this->manager()->getTotalEntry();
	}
	public function manager(){
		$rf = new ReflectionClass($this->manager_class);
		$manager = $rf->newInstance($this);
		return $manager;
	}
	public function getFriendlyDateInfoAttribute(){
		return $this->manager()->getFriendlyDateInfo();
	}
	public function getRankListAttribute(){
		return $this->_rank_list;
	}
	public function loadRankList(){
		$this->_rank_list = $this->manager()->getRankList();
	}
	public function getDates(){
		return ['start_date','end_date','created_at','updated_at'];
	}

}