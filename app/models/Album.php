<?php
class Album{
	public $title;
	public $date_range;
	public $feature_photos;
	public $user_id;
	public $year;
	public $month;
	public $photo_count;
	public static function createFromRange($user, $range1, $range2){
		$album = new Album;
		$album->title = "Photos of ".$range1->format('F, Y');
		$album->date_range = [$range1->toDateTimeString(), $range2->toDateTimeString()];
		$album->year = $range1->year;
		$album->month = $range1->format('F');
		$album->user_id = $user->id;
		$photos = $user->photos()->betweenDates($album->date_range);
		$album->photo_count = $photos->count();
		$album->feature_photos = $photos->orderBy("id","desc")->take(3)->get();
		return $album;
	}
}