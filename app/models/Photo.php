<?php
/*
DB fields
---------
id
type
status
user_id
caption
in_progress
src
promotes_count
view_count
points
created_at
updated_at
*/
class Photo extends Eloquent{
	protected $table = "photos";
	const TYPE_PP = "profile_pic";
	const TYPE_FEED = "feed";
	const STATUS_PUBLISHED = "published";
	const STATUS_DRAFT = "draft";
	protected $fillable = array('user_id','caption','src', 'type', "status","in_progress");
	protected $appends = array('original_url', 'is_promoted');
	public function getIsPromotedAttribute(){
		if(!Token::$current){
			return false;
		}
		if($this->promotes()->where("user_id","=",Token::$current->user->id)->count()>0){
			return true;
		}else{
			return false;
		}
	}
	public function badges(){
		return $this->morphMany('Badge','winner');
	}
	public function scopePublished($query){
		return $query->where("status",'=',Photo::STATUS_PUBLISHED)->where('in_progress','=', false);
	}
	public function scopeBetweenDates($query, $date_range){
		return $query->whereBetween('created_at',$date_range);
	}
	public function getInProgressAttribute(){
		return $this->attributes["in_progress"]==1?true:false;
	}
	public function getOriginalUrlAttribute(){
		return url($this->src);
	}
	public function user(){
		return $this->belongsTo('User','user_id');
	}
	public function votes(){
		return $this->morphMany('Vote','target');
	}
	public function promotes(){
		return $this->votes()->where('type','=',Vote::TYPE_PROMOTES);
	}
}